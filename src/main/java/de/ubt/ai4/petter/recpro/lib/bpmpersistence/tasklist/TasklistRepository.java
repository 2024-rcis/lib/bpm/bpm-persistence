package de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface TasklistRepository extends JpaRepository<Tasklist, Long> {
    List<Tasklist> getByAssigneeIdOrderByDateDesc(String assigneeId);
}
