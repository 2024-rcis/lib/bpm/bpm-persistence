package de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessInstanceRepository extends JpaRepository<ProcessInstance, Long> {
    ProcessInstance findByBpmsProcessInstanceId(String processInstanceId);
}
