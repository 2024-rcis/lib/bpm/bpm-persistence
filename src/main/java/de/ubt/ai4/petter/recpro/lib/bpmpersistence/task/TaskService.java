package de.ubt.ai4.petter.recpro.lib.bpmpersistence.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.TaskState;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.task.BpmsTaskService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Map;

@Service
@AllArgsConstructor
public class TaskService {
    private TaskPersistenceService persistenceService;
    private BpmsTaskService bpmsTaskService;

    public Task claim(Task task, String assigneeId) {
        task.setAssigneeId(assigneeId);
        Task result = this.save(task, TaskState.ASSIGNED);
        bpmsTaskService.claim(task.getTaskId(), assigneeId);
        return result;
    }

    public Task start(Task task) {
        Task result = this.save(task, TaskState.IN_PROGRESS);
        bpmsTaskService.start(result.getTaskId());
        return result;
    }

    public Task complete(Task task) {
        Task result = this.save(task, TaskState.COMPLETED);
        bpmsTaskService.complete(result.getTaskId());
        return result;
    }

    public Task unclaim(Task task) {
        task.setAssigneeId(null);
        Task result = this.save(task, TaskState.OPEN);
        bpmsTaskService.unclaim(result.getTaskId());
        return result;
    }
    public Task cancel(Task task) {
        Task result = this.save(task, TaskState.CANCELLED);
        bpmsTaskService.cancel(result.getTaskId());
        return result;
    }

    public Task stop(Task task) {
        return this.save(task, TaskState.PENDING);
    }

    private Task save(Task task, TaskState state) {
        task.setState(state);
        task.setEditDate(Instant.now());
        return persistenceService.saveAndFlush(task);
    }

    public void setAttributes(String taskId, Map<String, Object> attributes) {
        bpmsTaskService.setAttributes(taskId, attributes);
    }

    public Task getLatestByUserId(String userId) {
        return persistenceService.findLastByUserId(userId);
    }
}
