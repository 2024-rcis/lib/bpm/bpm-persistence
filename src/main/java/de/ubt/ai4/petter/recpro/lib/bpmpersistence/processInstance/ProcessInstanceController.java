package de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/recpro/bpm/execution/process/")
@AllArgsConstructor
public class ProcessInstanceController {
    private ProcessInstanceService processService;

    @PostMapping("/start/{processId}")
    public void start(@PathVariable String processId) {
        this.processService.startProcess(processId);
    }
}
