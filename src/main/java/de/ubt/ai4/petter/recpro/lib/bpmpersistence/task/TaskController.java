package de.ubt.ai4.petter.recpro.lib.bpmpersistence.task;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Task;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/recpro/bpm/execution/task/")
@AllArgsConstructor
class TaskController {
    private TaskService taskService;

    @PostMapping("/claim")
    public Task claim(@RequestBody Task task, @RequestHeader("X-User-ID") String assigneeId ) {
        return taskService.claim(task, assigneeId);
    }

    @PostMapping("/start")
    public Task start(@RequestBody Task task) {
        return taskService.start(task);
    }

    @PostMapping("/cancel")
    public Task cancel(@RequestBody Task task ) {
        return taskService.cancel(task);
    }

    @PostMapping("/unclaim")
    public Task unclaim(@RequestBody Task task) {
        return taskService.unclaim(task);
    }


    @PostMapping("/complete")
    public Task complete(@RequestBody Task task) {
        return taskService.complete(task);
    }

    @PostMapping("/stop")
    public Task stop(@RequestBody Task task) {
        return taskService.stop(task);
    }

    @GetMapping("/getLatestByUserId/{userId}")
    public Task getLatestByUserId(@PathVariable String userId) {
        return taskService.getLatestByUserId(userId);
    }
}
