package de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@MappedSuperclass
public class BpmElementInstance implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String assigneeId;
    private Double likeliness;
    private String recproElementId;
    private String recproElementInstanceId;

    public BpmElementInstance copy() {
        BpmElementInstance copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }
}
