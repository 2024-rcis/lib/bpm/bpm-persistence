package de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.task.TaskHelperService;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTasklist;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;

@AllArgsConstructor
@Service
class TasklistHelperService {
//    private TasklistPersistenceService persistenceService;
    private TaskHelperService taskService;
    public Tasklist fromAssigneeIdAndBpmsTasklist(String assigneeId, BpmsTasklist bpmsTasklist) {
//        Tasklist db = persistenceService.getLatestByAssigneeId(assigneeId);
        return this.fromBpmsTasklist(bpmsTasklist);
    }

    private Tasklist fromBpmsTasklist(BpmsTasklist tasklist) {
        Tasklist result = new Tasklist();
        result.setId(-1L);
        result.setAssigneeId(tasklist.getAssignee());
        result.setDate(Instant.now());
        result.setTasks(taskService.fromBpmsTask(tasklist.getTasks()));
        return result;
    }
}
