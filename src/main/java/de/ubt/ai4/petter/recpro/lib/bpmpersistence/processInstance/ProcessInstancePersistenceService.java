package de.ubt.ai4.petter.recpro.lib.bpmpersistence.processInstance;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.ProcessInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProcessInstancePersistenceService {
    private ProcessInstanceRepository repository;

    public List<ProcessInstance> getAll() {
        return repository.findAll();
    }

    public ProcessInstance getById(Long id) {
        return repository.findById(id).orElse(new ProcessInstance());
    }

    public ProcessInstance getByProcessInstanceId(String processInstanceId, String processId) {
        ProcessInstance instance = repository.findByBpmsProcessInstanceId(processInstanceId);
        if (instance == null) {
            instance = new ProcessInstance();
            instance.setBpmsProcessInstanceId(processInstanceId);
            instance.setRecproElementInstanceId(processInstanceId);
            instance.setRecproElementId(processId);
        }
        return instance;
    }

    public ProcessInstance saveAndFlush(ProcessInstance processInstance) {
        return this.repository.saveAndFlush(processInstance);
    }
}
