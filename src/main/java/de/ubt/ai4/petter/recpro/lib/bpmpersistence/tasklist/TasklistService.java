package de.ubt.ai4.petter.recpro.lib.bpmpersistence.tasklist;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.model.BpmsTasklist;
import de.ubt.ai4.petter.recpro.lib.bpms.execution.service.tasklist.BpmsTasklistService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TasklistService {
    private TasklistPersistenceService persistenceService;
    private BpmsTasklistService tasklistService;
    private TasklistHelperService tasklistHelper;

    public Tasklist save(Tasklist tasklist) {
        return persistenceService.saveAndFlush(tasklist);
    }

    public Tasklist getTasklist(String assigneeId) {
        BpmsTasklist tasklist = tasklistService.getTasklist(assigneeId);
        return tasklistHelper.fromAssigneeIdAndBpmsTasklist(assigneeId, tasklist);
    }

    public Tasklist overwrite(Tasklist tasklist) {
        return persistenceService.overwrite(tasklist);
    }

    public Tasklist getByAssigneeId(String assigneeId) {
        BpmsTasklist tasklist = tasklistService.getTasklistByAssignee(assigneeId);
        return tasklistHelper.fromAssigneeIdAndBpmsTasklist(assigneeId, tasklist);
    }

    public Tasklist getLatestByAssigneeId(String assigneeId) {
        return persistenceService.getLatestByAssigneeId(assigneeId);
    }

    public Tasklist getById(Long id) {
        return persistenceService.getById(id);
    }
}
