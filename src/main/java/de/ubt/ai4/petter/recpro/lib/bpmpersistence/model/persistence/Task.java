package de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.SerializationUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
public class Task extends BpmElementInstance {
    private String taskId;
    private int position = -1;

    @Enumerated(EnumType.STRING)
    private TaskState state;
    private String activityId;
    private String processId;
    private int priority;
    private Instant createDate;
    private Instant dueDate;
    private Instant editDate;
    private String executionId;
    private String processInstanceId;

    @Override
    public Task copy() {
        Task copy = SerializationUtils.clone(this);
        copy.setId(null);
        return copy;
    }

    public static List<Task> copy(List<Task> tasks) {
        if (tasks.isEmpty()) {
            return new ArrayList<>();
        }
        return tasks.stream().map(task -> task.copy()).toList();
    }
}
