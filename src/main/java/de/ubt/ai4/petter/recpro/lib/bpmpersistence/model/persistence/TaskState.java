package de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence;

public enum TaskState {
    OPEN, ASSIGNED, IN_PROGRESS, PENDING, COMPLETED, CANCELLED, DELETED
}
